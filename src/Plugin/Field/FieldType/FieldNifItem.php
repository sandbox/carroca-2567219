<?php

/**
 * @file
 * Contains \Drupal\field_nif\Plugin\Field\FieldType\FieldNifItem.
 */

namespace Drupal\field_nif\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'telephone' field type.
 *
 * @FieldType(
 *   id = "field_nif",
 *   label = @Translation("NIF/DNI/CIF"),
 *   description = @Translation("This field stores a NIF/DNI/DIF in the database."),
 *   category = @Translation("Text"),
 *   default_widget = "field_nif_widget",
 *   default_formatter = "field_nif_formatter"
 * )
 */
class FieldNifItem extends FieldItemBase {

    /**
     * {@inheritdoc}
     */
    public static function defaultStorageSettings() {

//        $options = array(
//            static::NIF_TYPE => t('NIF'),
//            static::CIF_TYPE => t('CIF'),
//            static::NIE_TYPE => t('NIE'),
//        );
        return array(
//            'allowed_documents_list' => $options,
//            'allowed_documents_default' => array_keys($options),
                ) + parent::defaultStorageSettings();
    }

    /**
     * Value allowing nif documents.
     */
    const NIF_TYPE = 'nif';

    /**
     * Value allowing cif documents.
     */
    const CIF_TYPE = 'cif';

    /**
     * Value allowing nie documents.
     */
    const NIE_TYPE = 'nie';

    /**
     * {@inheritdoc}
     */
    public static function schema(FieldStorageDefinitionInterface $field_definition) {
        $columns = array(
            'nif' => array(
                'description' => 'Numeric part of the NIF',
                'type' => 'varchar',
                'length' => 9,
            ),
            'number' => array(
                'description' => 'Numeric part of the NIF',
                'type' => 'varchar',
                'length' => 8,
            ),
            'first_letter' => array(
                'description' => 'First letter of the NIF/CIF/NIE',
                'type' => 'varchar',
                'length' => 1,
            ),
            'last_letter' => array(
                'description' => 'Last letter of the NIF/CIF/NIE',
                'type' => 'varchar',
                'length' => 1,
            ),
            'type' => array(
                'description' => 'Type of the number, NIF/CIF/NIE',
                'type' => 'varchar',
                'length' => 10,
            ),
        );

        $schema = array(
            'columns' => $columns,
            'indexes' => array(),
            'foreign keys' => array(),
        );

        return $schema;
    }

    /**
     * {@inheritdoc}
     */
    public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
        $element = array();

        $element['allowed_documents'] = array(
            '#title' => t('Allowed documents'),
            '#description' => t('Choose the supported documents.'),
            '#type' => 'checkboxes',
            '#disabled' => $has_data,
           // '#default_value' => $this->getSetting('allowed_documents_default'),
            '#default_value' => array( 1 => 'nif', 2 => 'cif'),
            '#options' => array(
        'nif' => t('NIF'),
        'cif' => t('CIF'),
        'nie' => t('NIE'),   
        )
            
        );

        return $element;
    }

    /**
     * {@inheritdoc}
     */
    public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
        $properties['nif'] = DataDefinition::create('string')
                ->setLabel(t('NIF'));
        $properties['number'] = DataDefinition::create('string')
                ->setLabel(t('NIF/DNI/CIF'));

        $properties['first_letter'] = DataDefinition::create('string')
                ->setLabel(t('NIF/DNI/CIF'));

        $properties['last_letter'] = DataDefinition::create('string')
                ->setLabel(t('NIF/DNI/CIF'));

        $properties['type'] = DataDefinition::create('string')
                ->setLabel(t('NIF/DNI/CIF'));

        return $properties;
    }

    /**
     * {@inheritdoc}
     */
    public function isEmpty() {
        $value = $this->get('nif')->getValue();
        return $value === NULL || $value === '';
    }

}
