<?php

/**
 * @file
 * Contains \Drupal\field_nif\Plugin\Field\FieldFormatter\FieldNifFormatter.
 */

namespace Drupal\field_nif\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'field_nif' formatter.
 *
 * @FieldFormatter(
 *   id = "field_nif_formatter",
 *   label = @Translation("Field nif"),
 *   field_types = {
 *     "field_nif"
 *   }
 * )
 */
class FieldNifFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    $elements = array();

    // The ProcessedText element already handles cache context & tag bubbling.
    // @see \Drupal\filter\Element\ProcessedText::preRenderText()

    foreach ($items as $delta => $item) {
      $elements[$delta] = array(
        '#type' => 'processed_text',
        '#text' => $item->nif,
        '#format' => $item->format,
        '#langcode' => $item->getLangcode(),
      );
    }

    return $elements;
  }

}
