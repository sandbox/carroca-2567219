<?php

/**
 * @file
 * Contains
  \Drupal\field_nif\Plugin\Field\FieldWidget\NifDefaultWidget.
 */

namespace Drupal\field_nif\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormState;
use Drupal\Core\Form\FormStateInterface;

/**
 *
 * @FieldWidget(
 *   id = "field_nif_widget",
 *   label = @Translation("NIF/DNI/CIF value"),
 *   field_types = {
 *     "field_nif"
 *   },
 *   multiple_values = FALSE,
 * )
 */
class FieldNifDefaultWidget extends WidgetBase {

    /**
     * {@inheritdoc}
     */
    public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
        $element['nif'] = array(
            '#type' => 'textfield',
            '#default_value' => isset($items[$delta]->nif) ? $items[$delta]->nif : NULL,
            '#title' => t('NIF/DNI/CIF'),
            '#size' => 9,
        );
        // Add the elements stored in db.
        $element['number'] = array(
            '#type' => 'hidden',
        );
        $element['first_letter'] = array(
            '#type' => 'hidden',
        );
        $element['last_letter'] = array(
            '#type' => 'hidden',
        );
        $element['type'] = array(
            '#type' => 'hidden',
        );
        $element += array(
            '#element_validate' => array(array($this, 'validateFormElement')),
        );
        $element['allowed_documents'] = parent::getFieldSetting('allowed_documents');
        return $element;
    }

    /**
     * Validate handler to check if the number is correct and store the values
     * splitted into their correct fields in the database.
     */

    /**
     * Form element validation handler for URL alias form element.
     *
     * @param array $element
     *   The form element.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   The form state.
     */
    public static function validateFormElement(array &$element, FormStateInterface $form_state) {
        if (!empty($element['nif']['#value'])) {
            if (strlen($element['nif']['#value']) <> 9) {
                $form_state->setError($element['nif'], 'NIF/CIF/NIE number needs to be 9 characters long');
            } else {
                // Load supported document formats 
                $supported_documents = $element['allowed_documents'];
                // Load the utilities for getting the NIF splitted.
                module_load_include('inc', 'field_nif', 'field_nif.utils');
                $nif_parts = _field_nif_validate_dni_cif_nie($element['nif']['#value'], $supported_documents);
                if ($nif_parts) {
                    $form_state->setValueForElement($element['number'], $nif_parts['number']);
                    $form_state->setValueForElement($element['type'], isset($nif_parts['type']) ? $nif_parts['type'] : '');
                    $form_state->setValueForElement($element['first_letter'], isset($nif_parts['first_letter']) ? $nif_parts['first_letter'] : '');
                    $form_state->setValueForElement($element['last_letter'], isset($nif_parts['last_letter']) ? $nif_parts['last_letter'] : '');
                } else {
                    $form_state->setError($element['nif'], 'NIF/CIF/NIE number is not correct');
                }
            }
        }
    }
}
